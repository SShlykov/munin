%{
  configs: [
    %{
      name: "default",
      files: %{
        included: ["lib/", "src/", "apps/"],
        excluded: ["lib/munin.ex"]
      },
      checks: [
        {Credo.Check.Refactor.MapInto, false},
        {Credo.Check.Warning.LazyLogging, false},
        {Credo.Check.Design.TagTODO, false},
        {Credo.Check.Warning.ApplicationConfigInModuleAttribute, false},
        {Credo.Check.Refactor.CyclomaticComplexity, max_complexity: 20}
      ]
    }
  ]
}
