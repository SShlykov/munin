defmodule Munin.Service.Global do
  @moduledoc """
  This service is about connecting to key value store with services info
  Attrs:
  * global_node_addr - node name (eg. :"global@172.25.78.153")
  * service - any name (eg. "thor")

  Usage:

      defmodule MyApp.GStore do
        use Munin.Service.Global,
          global_node_addr: :"global@172.25.78.153",
          service: "my_app"
      end

  and add to the application to init GStore task

      cheldrens: [
        ...,
        MyApp.GStore
        ...
      ]

  # then in any module of app

      iex> alias MyApp.GStore
      iex> GStore.save(:hello, "world")
      :ok
      iex> GStore.get(:hello)
      "world"
      iex> GStore.remove(:hello)
      :ok

  """
  @type module_vars :: [service: String.t(), params: map()]

  @spec __using__(module_vars) :: {:__block__, [], list()}
  defmacro __using__(service: service, params: params) when is_binary(service) do
    quote do
      use Agent

      @spec start_link(any) :: {:ok, pid}
      def start_link(init_val) do
        :io.format("starting service: #{unquote(service)}\n", [])
        init_params =
          %{machine_addr: Munin.fetch_machine_addr()}
          |> Map.merge(Enum.into(init_val, %{}))
          |> Map.merge(unquote(params))
          |> Map.merge(%{
              datetime_started: NaiveDateTime.local_now(),
              global_node_addr: Keyword.get(init_val, :global_node_addr, :"global@172.25.78.153"),
              service: unquote(service)
          })

        res = Agent.start_link(fn -> init_params end, name: __MODULE__)
        :timer.sleep(:timer.seconds(2))
        save(:init_params, init_params)
        :io.format("global communication info sended\n", [])
        :io.format("~n", [])
        res
      end

      def get_global, do: Agent.get(__MODULE__, & &1) |> Map.get(:global_node_addr, :"global@172.25.78.153")

      defp save(map) when is_map(map) do
        Enum.each(map, fn {key, value} -> save(key, value) end)
      end

      @doc """
      Stores data to global storage

      ## Examples

          iex> save(key, value)

          writes data to global store connected to current service

          iex> save(service, key, value)

          writes data to global store connected to service
      """
      def save(key, value) do
        :rpc.call(get_global(), GlobalNode.ServicesSupervisor, :store, [
          unquote(service),
          key,
          value
        ])
      end

      def save(service, key, value) do
        :rpc.call(get_global(), GlobalNode.ServicesSupervisor, :store, [
          service,
          key,
          value
        ])
      end

      @doc """
      Writes data into agent

      ##Examples

          iex> write_agent(writtable)

          overwrites data

          iex> write_agent(key, value)

          push key value pair into agent
      """
      def write_agent(writtable), do: Agent.update(__MODULE__, fn _ -> writtable end)
      def write_agent(key, value), do: Agent.update(__MODULE__, fn map -> Map.put(map, key, value ) end)

      @doc """
      Reads agent data

      ##Examples

          iex> read_agent()

          read all agent data
      """
      def read_agent,  do: Agent.get(__MODULE__, & &1)

      @doc """
      Resend init params to global store

      ## Examples

          iex> resend_info()

      """

      def resend_info() do
        init_params = read_agent()
        save(:init_params, init_params)
        init_params
      end

      @doc """
      Gettings data over services/current store

      ## Examples

          iex> get(service, key)

          where service is  specific service name registred in global/local network

          iex> get(key)

          takes data from global store connected to current service
      """
      def get(service, key) do
        :rpc.call(get_global(), GlobalNode.ServicesSupervisor, :value, [service, key])
      end

      def get(key) do
        :rpc.call(get_global(), GlobalNode.ServicesSupervisor, :value, [unquote(service), key])
      end


      @doc """
      Removes data over services/current store

      ## Examples

          iex> remove(service, key)

          specific service name registred in global/local network

          iex> remove(key)

          removes data from global store connected to current service
      """
      def remove(service, key) do
        :rpc.call(get_global(), GlobalNode.ServicesSupervisor, :remove, [service, key])
      end

      def remove(key) do
        :rpc.call(get_global(), GlobalNode.ServicesSupervisor, :remove, [unquote(service), key])
      end
    end
  end
end
