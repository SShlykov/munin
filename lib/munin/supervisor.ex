defmodule Munin.FSM.Supervisor do
  @moduledoc false
  use DynamicSupervisor
  @supervisor_name :munin_fsms
  alias Munin.FSM.ServicesRegistry

  @spec stop(atom(), atom(), atom()) :: :ok | {:error, :not_exist | :not_found}
  def stop(service_name, app_name, global_node_addr) do
    case find(service_name, app_name, global_node_addr) do
      {:ok, pid} ->
        DynamicSupervisor.terminate_child(__MODULE__, pid)

      e ->
        e
    end
  end

  @spec create(module, atom(), atom(), atom()) :: {:error, :already_exists} | {:ok, pid}
  def create(adapter, service_name, app_name, global_node_addr) do
    case find(service_name, app_name, global_node_addr) do
      {:ok, _pid} ->
        {:error, :already_exists}

      {:error, :not_exist} ->
        {:ok, pid} = start(adapter, service_name, app_name, global_node_addr)
        spawn(fn ->
          :timer.sleep(:timer.seconds(5))
          :gen_statem.call(service_name, :check_inited)
        end)
        {:ok, pid}
    end
  end

  #
  # Серверные функции
  #

  def start_link(_opts) do
    DynamicSupervisor.start_link(__MODULE__, [], name: @supervisor_name)
  end

  @impl true
  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one, extra_arguments: [])
  end

  def stop_supervisor(timeout \\ 5000) do
    Supervisor.stop(__MODULE__, :normal, timeout)
  end

  defp start(adapter, service_name, app_name, global_node_addr) do
    name = {:via, Registry, {ServicesRegistry, {service_name, app_name, global_node_addr}}}

    DynamicSupervisor.start_child(@supervisor_name, {adapter, name})
  end

  defp find(service_name, app_name, global_node_addr) do
    case Registry.lookup(ServicesRegistry, {service_name, app_name, global_node_addr}) do
      [] -> {:error, :not_exist}
      [{pid, nil}] -> {:ok, pid}
    end
  end
end
