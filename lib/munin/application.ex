defmodule Munin.Application do
  @moduledoc """
  The application runs `Supervisor` and its registry for it's childs (communicators)
  """
  use Application

  @doc false
  @spec start(any, any) :: {:error, any} | {:ok, pid}
  def start(_, _) do
    children = [
      {Registry, keys: :unique, name: Munin.FSM.ServicesRegistry},
      Munin.FSM.Supervisor
    ]

    opts = [strategy: :one_for_one, name: Munin.FSM.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
