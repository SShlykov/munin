defmodule EnvReader do

  def read(root_path) do
    root_path
    |> read(:direct)
    |> read(:rejoin)
  end

  def read(root_path, :direct) do
    Path.join(File.cwd!, root_path)
    |> File.read!()
    |> String.split("\n")
    |> Enum.map(&String.trim/1)
    |> Enum.reject(& &1 == "")
    |> Enum.map(&Code.eval_string/1)
    |> Enum.map(&elem(&1, 1))
    |> List.flatten
  end

  def read(list, :rejoin) when is_list(list) do
    list
    |> Enum.group_by(&map_head/1, &map_body/1)
    |> Enum.map(fn
      {k, v} when is_list(v) ->
        if(is_tuple(hd(v)), do: {k, Enum.into(read(v, :rejoin), %{})}, else: {k, hd(v)})
      {k, v}  ->
        {k, v}
    end)
  end
  def read(list, :rejoin), do: list

  def map_head({key, _value}) do
    if length(String.split("#{key}", "_")) > 1 do
      String.split("#{key}", "_")
      |> hd()
      |> String.to_atom()
    else
      key
    end
  end
  def map_body({key, value}) do
    if length(String.split("#{key}", "_")) > 1 do
      {
        set_key(key),
        value
      }
    else
      value
    end
  end

  defp set_key(key) do
    if length(String.split("#{key}", "_")) > 1 do
      String.split("#{key}", "_")
      |> tl()
      |> Enum.join("_")
      |> String.to_atom()
    else
      key
    end
  end
end
