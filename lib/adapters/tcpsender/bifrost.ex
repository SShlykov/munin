defmodule Munin.Bifrost do
  @moduledoc """
  The machine that controls access to external services.
  Should the connection be broken, should redirect the event stream to a file.

  The machine starts up through its supervisor.
  Uses the interface of `gen_statem`
  """
  @behaviour :gen_statem
  @retry_timeout 10_000
  @send_timeout 10_000
  require Logger
  alias Munin.Utils

  @typedoc false
  @type action :: any()
  @typedoc false
  @type prev_state :: any()
  @typedoc false
  @type event :: :gen_statem.event_type()
  @typedoc false
  @type fsm_data :: any

  @spec start_link({:via, atom(), {atom(), {atom(), atom()}}}) :: :gen_statem.start_ret()
  @spec start_link(list(:gen_statem.start_opt())) :: :gen_statem.start_ret()
  @spec start({:via, atom(), {atom(), {atom(), atom()}}}) :: :gen_statem.start_ret()
  @spec stop(atom) :: :ok
  @spec init({atom(), atom()}) :: {:ok, :connected, {atom(), atom()}}
  @spec callback_mode :: :handle_event_function
  @spec handle_event(event(), action(), prev_state(), fsm_data()) ::
          :gen_statem.handle_event_result()
  @spec child_spec(any) :: %{
          id: __MODULE__,
          restart: :permanent,
          start: {__MODULE__, :start_link, any()},
          type: :worker
        }

  @doc false
  def start_link(name), do: start(name)

  @doc false
  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]},
      type: :worker,
      restart: :permanent
    }
  end

  @doc false
  def start({:via, Registry, {_registry_name, {service_name, app_name}}}) do
    :gen_statem.start_link({:local, service_name}, __MODULE__, {service_name, app_name}, [])
  end

  @doc false
  def stop(name), do: :gen_statem.stop(name)

  @doc false
  @impl :gen_statem
  def init({service_name, app_name}), do: {:ok, :connected, {service_name, app_name}}

  @doc false
  @impl :gen_statem
  def callback_mode, do: :handle_event_function

  @doc """
  Final implementation of event handling.

  FSM has 3 states -
    * connected - sends data via usual connection
    * disconnected - stores important functions to file
    * sending - works as connected but sends stored functions
  """
  @impl :gen_statem
  def handle_event({:call, from}, {:unhandled, func_type, params}, :connected, {service_name, app_name}) do
    Utils.write_file(func_type, params, service_name, app_name)

    {:next_state, :disconnected, {service_name, app_name},
     [
       {:state_timeout, @retry_timeout, :retry_connect},
       {:reply, from, :error}
     ]}
  end
  def handle_event({:call, from}, {:unhandled, func_type, params}, :sending, {service_name, app_name}) do
    Utils.write_file(func_type, params, service_name, app_name)

    {:next_state, :disconnected, {service_name, app_name},
     [
       {:state_timeout, @retry_timeout, :retry_connect},
       {:reply, from, :error}
     ]}
  end

  def handle_event({:call, from}, {:unhandled, func_type, params}, :disconnected, {service_name, app_name}) do
    Utils.write_file(func_type, params, service_name, app_name)

    {:keep_state, {service_name, app_name}, [{:reply, from, :ok}]}
  end

  def handle_event(:state_timeout, :retry_connect, :disconnected, {service_name, app_name}) do
    case :global.whereis_name(service_name) do
      :undefined ->
        {:keep_state, {service_name, app_name}, [{:state_timeout, @retry_timeout, :retry_connect}]}

      _ ->
        spawn(fn ->
          Utils.get_munin_module(service_name, app_name)
          |> apply(:list_logs, [])
          |> Utils.do_send(service_name, app_name)

          :gen_statem.call(service_name, :sended)
        end)

        {:next_state, :sending, {service_name, app_name}, [{:state_timeout, @send_timeout, :do_send}]}
    end
  end

  def handle_event({:call, from}, {:call, params}, :disconnected, {service_name, app_name}) do
    {:unhandled_error, err} = Utils.do_error(service_name, "unavaliable service", params, "call")
    Utils.write_file("call", err, service_name, app_name)
    {:keep_state, {service_name, app_name}, [{:reply, from, err}]}
  end

  def handle_event({:call, from}, {:call, params}, _state, {service_name, app_name}) do
    case Utils.do_call(service_name, params) do
      {:unhandled_error, err} ->
        Utils.write_file("call", err, service_name, app_name)

        {:next_state, :disconnected, {service_name, app_name},
         [
           {:state_timeout, @retry_timeout, :retry_connect},
           {:reply, from, err}
         ]}

      any ->
        {:keep_state, {service_name, app_name}, [{:reply, from, any}]}
    end
  end

  def handle_event({:call, from}, :sended, :sending, {service_name, app_name}),
    do: {:next_state, :connected, {service_name, app_name}, [{:reply, from, :ok}]}

  def handle_event({:call, from}, :service_name, _content, {service_name, app_name}),
    do: {:keep_state, {service_name, app_name}, [{:reply, from, service_name}]}

  def handle_event({:call, from}, :module_name, _content, {service_name, app_name}),
    do: {:keep_state, {service_name, app_name}, [{:reply, from, Utils.get_munin_module(service_name, app_name)}]}

  def handle_event({:call, from}, :list_log_files, _content, {service_name, app_name}),
    do:
      {:keep_state, {service_name, app_name},
       [{:reply, from, apply(Utils.get_munin_module(service_name, app_name), :list_logs, [])}]}

  def handle_event({:cast, from}, {:cast, params}, :disconnected, {service_name, app_name}) do
    {:unhandled_error, err} = Utils.do_error(service_name, "unavaliable service", params, "cast")
    Utils.write_file("cast", err, service_name, app_name)
    {:keep_state, {service_name, app_name}, [{:reply, from, err}]}
  end

  def handle_event({:cast, from}, {:cast, params}, _state, {service_name, app_name}) do
    resp =
      case Utils.do_cast(service_name, params) do
        {:unhandled_error, err} ->
          Utils.write_file("cast", err, service_name, app_name)

        any ->
          any
      end

    {:keep_state, {service_name, app_name}, [{:reply, from, resp}]}
  end

  def handle_event({:call, from}, event, content, state),
    do: {:keep_state, state, [{:reply, from, {:error, "invalid transaction event: #{inspect(event)}; content: #{inspect(content)};"}}]}

  def handle_event(_any, _event, _content, state), do: {:keep_state, state, []}
end
