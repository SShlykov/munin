defmodule Munin.FileWritter.Behaviour do
  @moduledoc """
  Munin Classic behaviour functions
  """
  @typedoc false
  @type file_strings :: list(binary()) | nil
  @typedoc false
  @type important_list :: list(atom())

  @spec list_logs(module) :: list
  @spec read_logs(module, any) :: file_strings()
  @spec read_logs_and_remove(module, binary) :: file_strings()
  @spec count_logs(module, binary) :: {:ok, number()}

  @callback important_list :: important_list()
  @callback read_file(binary) :: {:ok, binary}
  @callback write_file(binary, binary, binary) :: binary
  @callback read_file_and_remove(binary) :: {:ok, binary}
  @callback count_logs(binary) :: {:ok, number}

  @doc """
  When called in a module, uses a specific implementation for readind file in the log path folder and perses it to list of commands

  ## Example

      iex> read_logs(impl, file_name)
      [...]

  """
  def read_logs(impl, file_name) do
    with {:ok, file} <- impl.read_file("#{impl.logs_path}/#{file_name}"),
         data <- String.split(file, "\n", trim: true) do
      data
      |> Stream.reject(&is_nil/1)
      |> Stream.reject(&(&1 == ""))
      |> Stream.map(&Code.eval_string/1)
      |> Enum.map(& elem(&1, 0))
    end
  end

  @doc """
  Doing the same as `read_logs` but deletes file

  ## Example

      iex> read_logs_and_remove(impl, file_name)
      [...]

  """
  def read_logs_and_remove(impl, file_name) do
    with {:ok, file} <- impl.read_file_and_remove("#{impl.logs_path}/#{file_name}"),
         data <- String.split(file, "\n", trim: true) do
      data
      |> Stream.reject(&is_nil/1)
      |> Stream.reject(&(&1 == ""))
      |> Stream.map(&Code.eval_string/1)
      |> Enum.map(& elem(&1, 0))
    end
  end

  @doc """
  When called in a module, uses a specific implementation for checking the number of logs

  ## Example

      iex> count_logs(impl, filename)
      {:ok, 10}

  """
  def count_logs(impl, filename), do: impl.count_log(filename)

  @doc """
  When called in a module, uses a specific implementation for checking log files that stored

  ## Example

      iex> Munin.Classic.Behaviour.list_logs(impl)
      [...]

  """
  def list_logs(impl) do
    "#{impl.logs_path}"
    |> File.ls!()
    |> List.delete(".gitignore")
    |> Enum.filter(fn x -> String.match?(x, ~r/#{impl.module_name}/) end)
  end
end
