defmodule Munin.MixProject do
  use Mix.Project

  @source_url "https://gitlab.com/niac_vniizht/munin"
  @version "0.1.22"

  def project do
    [
      app: :munin,
      version: @version,
      elixir: "~> 1.22",
      start_permanent: Mix.env() == :prod,
      compilers: Mix.compilers(),
      deps: deps(),
      aliases: aliases(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: coveralls(),

      # Docs
      name: "Munin",
      source_url: @source_url,
      docs: docs(),
    ]
  end

  defp coveralls do
    [
      coveralls: :test,
      "coveralls.detail": :test,
      "coveralls.post": :test,
      "coveralls.html": :test
    ]
  end

  defp docs do
    [
      main: "Munin",
      source_ref: "v#{@version}",
      source_url: @source_url,
      groups_for_modules: [
        "FSM": [
          Munin.FSM
        ]
      ]
    ]
  end

  def application do
    [
      mod: {Munin, []},
      extra_applications: [:logger, :ex_doc]
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:excoveralls, "~> 0.10", only: :test, runtime: false},
      {:ex_doc, "~> 0.20"},
      {:poison, "~> 4.0"},
      {:morphix, "~> 0.8.0"}
    ]
  end

  defp aliases do
    [
      test: ["test"]
    ]
  end
end
